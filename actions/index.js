let nextTodoId = 0
export const addTodo = (text, index) => {
  return {
    type: 'ADD_TODO',
    id: nextTodoId++,
    text,
    index
  }
}

export const setVisibilityFilter = (filter) => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}

export const toggleTodo = (id) => {
  return {
    type: 'TOGGLE_TODO',
    id
  }
}
