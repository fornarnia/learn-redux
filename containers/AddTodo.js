import React from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../actions'

let AddTodo = ({ dispatch }) => {
  let input
  let index

  return (
    <div>
      <input ref={node => {
        input = node
      }} placeholder = 'text' />
      <input ref={node => {
        index = node
      }} placeholder = 'index' />
      <button onClick={() => {
        console.log(input.value)
        dispatch(addTodo(input.value, index.value))
        input.value = ''
        index.value = ''
      }}>
        Add Todo
      </button>
    </div>
  )
}
AddTodo = connect()(AddTodo)

export default AddTodo
